#include "clang/Driver/Options.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Lex/Lexer.h"

using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;

Rewriter rewriter;

class ExampleVisitor : public RecursiveASTVisitor<ExampleVisitor> {
private:
    ASTContext *astContext; // used for getting additional AST info
    SourceManager *SM;
    BeforeThanCompare<SourceLocation> isBefore; // For comparing SourceLocation
    SourceRange range[10];  // Keeping range of code where pragma is already added
    int ctr = 0;

public:
    explicit ExampleVisitor(CompilerInstance *CI) 
      : astContext(&(CI->getASTContext())), 
        SM(&(astContext->getSourceManager())),
        isBefore(*SM)
    {
        rewriter.setSourceMgr(*SM, astContext->getLangOpts());
        llvm::outs() << "Original File\n";
        rewriter.getEditBuffer(SM->getMainFileID()).write(llvm::outs());
        llvm::outs() << "******************************\n";
    }

    // __builtin_FUNCTION() return the name of the calling function
    virtual bool VisitStmt(Stmt *st, const char* str = __builtin_FUNCTION()) {
        // Ignore if the statement is in System Header files
        if(!st->getLocStart().isValid() || SM->isInSystemHeader(st->getLocStart()))
                return true;

        //llvm::errs() << "Called by " << str << "\n";

        if (dyn_cast<OMPParallelForDirective>(st) || 
            dyn_cast<OMPForDirective>(st)) {
            llvm::errs() << "OMP for directive found\n";
            for( Stmt *sub: st->children())
                if(sub && dyn_cast<CapturedStmt>(sub)) {
                    //llvm::errs() << "Captured Statement\n";
                    //range[ctr++] = sub->getSourceRange();
                    llvm::errs() << "Removing omp source\n";
                    const char *start = SM->getCharacterData(st->getLocStart());
                    SourceLocation e(Lexer::getLocForEndOfToken(st->getLocEnd(), 
                                0, *SM, astContext->getLangOpts()));
                    const char *end = SM->getCharacterData(e);
                    //const char *end = SM->getCharacterData(st->getLocEnd());
                    rewriter.RemoveText(st->getLocStart().getLocWithOffset(-8), 
                            end - start + 8);
                }
        } else if (ForStmt *f = dyn_cast<ForStmt>(st)) {
            llvm::errs() << "For found \n";
            //f->dumpPretty(*astContext);

            if(!is_for_parallel(f))
                return true;

            SourceLocation srcloc = getCodeLoc(f->getForLoc());
            srcloc.dump(*SM);
            llvm::errs() << "\n";

            for(int i=0; i<ctr; i++) {
                if(isBefore(srcloc, range[i].getEnd()) && 
                        !isBefore(srcloc, range[i].getBegin())) {
                    llvm::errs() << "In Range\n";
                    return true;
                }
            }
            std::string code = "#pragma omp parallel Myfor ";
            if(dyn_cast<ForStmt>(f->getBody())) 
                code = code + "collapse(2) ";
            code = code + "\n    ";

            if(rewriter.InsertTextBefore(srcloc, code))
                llvm::errs() << "Unable to insert Text\n";
            range[ctr++] = f->getSourceRange();
        }
        return true;
    }

private:
    // Function to get the location of the expanded code
    // If the code is a macro it'll expand the code and then return location
    SourceLocation getCodeLoc(SourceLocation src) {
        if(src.isFileID())
            return src;
        return SM->getExpansionLoc(src);
    }

    // Function to check if the for loop is parallelable or not
    bool is_for_parallel(ForStmt *st) {
        return true;
    }
};

class ExampleASTConsumer : public ASTConsumer {
private:
    ExampleVisitor *visitor; // doesn't have to be private

public:
    explicit ExampleASTConsumer(CompilerInstance *CI)
        : visitor(new ExampleVisitor(CI)) // initialize the visitor
    { }

    // override this to call our ExampleVisitor on the entire source file
    virtual void HandleTranslationUnit(ASTContext &Context) {
        /* we can use ASTContext to get the TranslationUnitDecl, which is
             a single Decl that collectively represents the entire source file */
        visitor->TraverseDecl(Context.getTranslationUnitDecl());
    }
};

class ExampleFrontendAction : public ASTFrontendAction {
public:
    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, 
            StringRef file) {
        // pass CI pointer to ASTConsumer
        return std::unique_ptr<ASTConsumer>(new ExampleASTConsumer(&CI)); 
    }
};

int main(int argc, const char **argv) {
    llvm::cl::OptionCategory ClangCheckCategory("clang-example options");
    // parse the command-line args passed to your code
    CommonOptionsParser op(argc, argv, ClangCheckCategory);

    // create a new Clang Tool instance (a LibTooling environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());

    // run the Clang Tool, creating a new FrontendAction
    int result = Tool.run(newFrontendActionFactory<ExampleFrontendAction>().get());

    // print out the rewritten source code ("rewriter" is a global var.)
    llvm::outs() << "******************************\n";
    llvm::outs() << "Modified File\n";
    FileID id = rewriter.getSourceMgr().getMainFileID();
    rewriter.getEditBuffer(id).write(llvm::outs());
    return result;
}
