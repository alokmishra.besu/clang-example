add_clang_executable(clang-example Example.cpp)

target_link_libraries(clang-example clangTooling)

install(TARGETS clang-example
        RUNTIME DESTINATION bin)
